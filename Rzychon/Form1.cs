﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Rzychon {
    public partial class Form1 : Form {
        private String path;
        private bool isPathLoaded = false;

        public Form1() {
            InitializeComponent();

            Font font = new System.Drawing.Font("Times", 16f, System.Drawing.FontStyle.Bold);

            chart.Series["mainSeries"].IsVisibleInLegend = false;
            chart.Series["parallelLineSeries"].IsVisibleInLegend = false;
            chart.Series["movedParallelLineSeries"].IsVisibleInLegend = false;
            chart.ChartAreas[0].AxisX.Minimum = 0;
            chart.ChartAreas[0].AxisY.LabelStyle.Font = font;
            chart.ChartAreas[0].AxisX.LabelStyle.Font = font;
            chart.ChartAreas[0].AxisX.TitleFont = font;
            chart.ChartAreas[0].AxisY.TitleFont = font;

            lengthBeforeText.TextChanged += new EventHandler(alloyTensile);
            sampleWidthText.TextChanged += new EventHandler(alloyTensile);
            sampleDepthText.TextChanged += new EventHandler(alloyTensile);
        }

        private int calibrateParallelLine = 5; // default 5

        private void alloyTensile(object sender, EventArgs e) {
            //List<double> strainData = new List<double>();
            List<double> forceData = new List<double>();
            List<double> gripData = new List<double>();
            List<double> gripDiffData = new List<double>();

            if (lengthBeforeText.Text.ToString().Length == 0) {
                return;
            }

            FileStream fs = File.OpenRead(path);
            StreamReader reader = new StreamReader(fs);

            double firstGrip = 0, currentGrip, previousForce = 0, previousGrip = 0, lastRisingForce = 0, 
                lastRisingForceGrip = 0, currentForce, firstForce = 0, gripDiff;
            double maxForce = Double.MinValue, maxGrip = Double.MinValue;
            double lastDiffBetweenForces = 0;
            bool isFirstGrip = true, lookForLastRisingForce = true;

            double sampleWidth = 0, sampleDepth = 0;
            double Ro2 = 0, Rm = 0, So = 0, A10 = 0; /*[mm2] */

            byte forceDiffCounter = 0;

            int lineCounter = 0;

            List<double[,]> points = new List<double[,]>();

            // Make sure chart is clear

            chart.Series["mainSeries"].Points.Clear();
            chart.Series["parallelLineSeries"].Points.Clear();
            chart.Series["movedParallelLineSeries"].Points.Clear();
            chart.ChartAreas[0].AxisX.Title = "Wydluzenie [mm]";
            chart.ChartAreas[0].AxisY.Title = "Sila [N]";

            try {
                double lengthBefore = Double.Parse(lengthBeforeText.Text.ToString());
                double pointTwoPercentageOfLengthBefore = 0.02 * lengthBefore;
                sampleWidth = Double.Parse(sampleWidthText.Text); 
                sampleDepth = Double.Parse(sampleDepthText.Text);

                while (!reader.EndOfStream) {
                    String line = reader.ReadLine();

                    if (line.Contains(';')) {
                        lineCounter++;                        

                        line = line.Replace(" ", "");
                        String[] values = line.Split(';');

                        if (!Double.TryParse(values[1], NumberStyles.Float, CultureInfo.InvariantCulture, out currentForce) ||
                            !Double.TryParse(values[2], NumberStyles.Float, CultureInfo.InvariantCulture, out currentGrip)) {
                            continue;
                        }

                        if (isFirstGrip) {
                            isFirstGrip = false;
                            firstGrip = Double.Parse(values[2], NumberStyles.Float, CultureInfo.InvariantCulture);
                            firstForce = Double.Parse(values[1], NumberStyles.Float, CultureInfo.InvariantCulture);

                            points.Add(new double[,] { { 0, firstForce } });
                            previousForce = firstForce;
                            previousGrip = firstGrip;
                        }

                        currentForce = Double.Parse(values[1], NumberStyles.Float, CultureInfo.InvariantCulture);
                        currentGrip = Double.Parse(values[2], NumberStyles.Float, CultureInfo.InvariantCulture);
                        gripDiff = currentGrip - firstGrip;

                        //strainData.Add(Double.Parse(values[0], NumberStyles.Float, CultureInfo.InvariantCulture));
                        forceData.Add(Double.Parse(values[1], NumberStyles.Float, CultureInfo.InvariantCulture));
                        //gripData.Add(Double.Parse(values[2], NumberStyles.Float, CultureInfo.InvariantCulture));
                        gripDiffData.Add(gripDiff);

                        // Find max force, used to calculate tensile strength
                        if (currentForce > maxForce) {
                            maxForce = currentForce;
                        }

                        // Find max grip, used to calculate length A
                        if (gripDiff > maxGrip) {
                            maxGrip = gripDiff;
                        }

                        if (lookForLastRisingForce) {
                            // This is to prevent from stopping when there is zero difference between grips
                            if (previousGrip - currentGrip > 0 || lineCounter > 50) {
                                if (currentForce - previousForce < lastDiffBetweenForces) {
                                    forceDiffCounter++;
                                    // This is an assumption that current force will not be smaller more than calibrateParallelLine
                                    // times before actually force delta is decreasing
                                    if (forceDiffCounter > calibrateParallelLine) {
                                        lookForLastRisingForce = false;
                                    }
                                } else {
                                    lastRisingForce = currentForce;
                                    lastRisingForceGrip = gripDiff;
                                    forceDiffCounter = 0;
                                }
                            }

                            lastDiffBetweenForces = currentForce - previousForce;
                            previousForce = currentForce;
                        }

                        previousGrip = currentGrip;

                        chart.Series["mainSeries"].Points.AddXY(gripDiff, currentForce);
                    }
                }

                points.Add(new double[,] { { lastRisingForceGrip, lastRisingForce } });

                // Line cutting two specified points
                double a = (points[1][0, 1] - points[0][0, 1]) / (points[1][0, 0] - points[0][0, 0]);
                double b = points[0][0, 1] - a * points[0][0, 0];

                chart.Series["parallelLineSeries"].Points.AddXY(0, a * 0 + b);
                chart.Series["parallelLineSeries"].Points.AddXY(0.6, a * 0.6 + b);

                // Look for common point for line and results from trial
                double Fo2 = double.MaxValue, tempForce = 0;

                for (int i = 0; i < lineCounter - 1; i++) {
                    tempForce = a * (gripDiffData[i] - pointTwoPercentageOfLengthBefore) + b + pointTwoPercentageOfLengthBefore;

                    if (Math.Abs(tempForce - forceData[i]) < Math.Abs(tempForce - Fo2) && tempForce <= forceData[i]) {
                        Fo2 = forceData[i];
                    }
                }

                chart.Series["movedParallelLineSeries"].Points.AddXY(0 + pointTwoPercentageOfLengthBefore, a * 0 + b + pointTwoPercentageOfLengthBefore);
                chart.Series["movedParallelLineSeries"].Points.AddXY(0.6 + pointTwoPercentageOfLengthBefore, a * 0.6 + b + pointTwoPercentageOfLengthBefore);

                So = sampleDepth * sampleWidth;
                Rm = maxForce / So;
                Ro2 = Fo2 / So;
                A10 = maxGrip / lengthBefore *100;

                double[] arrayValues = { So, Rm, Ro2, A10 };

                foreach (double var in arrayValues) {
                    if (Double.IsNaN(var)) {
                        throw new Exception();
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show("Blad podczas przetwarzania danych. Sprawdz poprawnosc danych oraz czy zostal wybrany poprawny typ proby.", "Blad!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            } finally {
                label1.Text = "Wytrzymałośc na rozciąganie Rm [MPa]";
                label2.Text = Math.Round(Rm, 3).ToString();
                label3.Text = "Granica plastycznosci Rp0.2 [MPa]";
                label4.Text = Math.Round(Ro2, 3).ToString();
                label5.Text = "Wydłużenie A10 [%]";
                label6.Text = Math.Round(A10, 3).ToString();
            }  
        }

        private void creep() {
            FileStream fs = File.OpenRead(path);
            StreamReader reader = new StreamReader(fs);

            bool firstLine = true;

            double startTime = 0, timeHours, strainInPercentage, baseValue = 36.5, predkoscPelzania = 0, odkszNatychmiastowe = 0;
            double timeRead, strainRead = 0, forceRead = 0;
            double e1 = 0, t1 = 0, e2 = 02, t2 = 0; // zmienne pomocnicze
            double averageForce = 0, srednicaProbki=1, polePrzekorjuProbki = 0, naprezenie = 0;
            double firstStrain = 0.0, modułYounga=0;
            // Make sure chart is clear
            chart.Series["mainSeries"].Points.Clear();
            chart.Series["parallelLineSeries"].Points.Clear();
            chart.Series["movedParallelLineSeries"].Points.Clear();
            chart.ChartAreas[0].AxisX.Title = "Czas [h]";
            chart.ChartAreas[0].AxisY.Title = "Odksztalcenie [%]";

            try {
                while (!reader.EndOfStream) {
                    String line = reader.ReadLine();

                    // Get rid of spaces and replace ',' with '.' to enable proper parsing
                    line = line.Replace(" ", "");
                    line = line.Replace(",", ".");
                    String[] values = line.Split('\t');

                    if (!Double.TryParse(values[0], NumberStyles.Float, CultureInfo.InvariantCulture, out strainRead) ||
                        !Double.TryParse(values[1], NumberStyles.Float, CultureInfo.InvariantCulture, out forceRead)) {
                        continue;
                    }
                       
                    if (firstLine) {
                        firstLine = false;
                        startTime = Double.Parse(values[2], NumberStyles.Float, CultureInfo.InvariantCulture);      
                    }
                 
                    if (firstStrain == 0.0) {
                        firstStrain = strainRead;
                    }
   
                    // Some of time values are corrupted, this should fix the issue
                    if (values[2].Contains("01.")) {
                        timeRead = Double.Parse(values[2].Replace("01.", ""), NumberStyles.Float, CultureInfo.InvariantCulture);
                    } else {
                        timeRead = Double.Parse(values[2], NumberStyles.Float, CultureInfo.InvariantCulture);
                    }

                    timeHours = timeRead - startTime;
                    strainInPercentage = (strainRead / baseValue) * 100;
                    averageForce = (averageForce + forceRead) / 2;
                    if (strainInPercentage >= 0.00001) {
                        e1 = strainRead;
                        t1 = timeHours * 3600;
                    }
                    if (strainInPercentage <= 0.00001 && timeHours >= 15) {
                        e2 = strainRead;
                        t2 = timeHours * 3600;
                    }

                    predkoscPelzania = (e2 - e1) / (t2 - t1);
    
                    chart.Series["mainSeries"].Points.AddXY(
                        timeHours,
                        strainInPercentage);
                }

                polePrzekorjuProbki = (srednicaProbki * srednicaProbki * Math.PI) / 4;
                naprezenie = averageForce /polePrzekorjuProbki ;
                modułYounga = naprezenie / firstStrain;
                odkszNatychmiastowe = naprezenie / modułYounga;

                double[] arrayValues = { polePrzekorjuProbki, naprezenie, modułYounga, odkszNatychmiastowe };

                foreach (double var in arrayValues) {
                    if (Double.IsNaN(var) || Double.IsInfinity(var)) {
                        throw new Exception();
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show("Blad podczas przetwarzania danych. Sprawdz czy zostal wybrany poprawny typ proby.", "Blad!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            } finally {
                label1.Text = "Odkształcenie całkowite [mm]";
                label2.Text = Math.Round(strainRead,5).ToString();
                label3.Text = "Prędkość pełzania w stanie ustalonym [mm/h]";
                label4.Text = Math.Round(Math.Abs(predkoscPelzania), 10).ToString();
                label5.Text = "Odkształcenie natychmastowe [mm] ";
                label6.Text = Math.Round(odkszNatychmiastowe, 10).ToString();
            }
        }

        private void selectFileBtn_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "All Files|*.*";
            DialogResult selected = openFileDialog.ShowDialog();
            if (selected == DialogResult.OK) {
                path = openFileDialog.FileName;
                filePath.Text = path;
                isPathLoaded = true;
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            if (isPathLoaded) {
                if (alloyTensileRadioButton.Checked) {
                    alloyTensile(null, null);
                } else if (creepRadioButton.Checked) {
                    creep();
                }
            } else {
                MessageBox.Show("Wczytaj plik!", "Blad!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e) {
            calibrateParallelLine = calibrateParallelLineTrackBar.Value;
            button1_Click(null, null);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e) {
            if (alloyTensileRadioButton.Checked) {
                calibrateParallelLineTrackBar.Visible = true;
                calibrateParallelLineLabel.Visible = true;
                lengthBeforeLabel.Visible = true;
                lengthBeforeText.Visible = true;
                sampleDepthText.Visible = true;
                sampleWidthText.Visible = true;
                sampleDepthLabel.Visible = true;
                sampleWidthLabel.Visible = true;
                sampleDimensionsLabel.Visible = true;
            } else {
                calibrateParallelLineTrackBar.Visible = false;
                calibrateParallelLineLabel.Visible = false;
                lengthBeforeLabel.Visible = false;
                lengthBeforeText.Visible = false;
                sampleDepthText.Visible = false;
                sampleWidthText.Visible = false;
                sampleDepthLabel.Visible = false;
                sampleWidthLabel.Visible = false;
                sampleDimensionsLabel.Visible = false;
            }
        }
    }
}
