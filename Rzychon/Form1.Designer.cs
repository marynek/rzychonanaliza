﻿namespace Rzychon
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.selectFileBtn = new System.Windows.Forms.Button();
            this.filePath = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.alloyTensileRadioButton = new System.Windows.Forms.RadioButton();
            this.creepRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.calibrateParallelLineTrackBar = new System.Windows.Forms.TrackBar();
            this.calibrateParallelLineLabel = new System.Windows.Forms.Label();
            this.lengthBeforeText = new System.Windows.Forms.TextBox();
            this.lengthBeforeLabel = new System.Windows.Forms.Label();
            this.sampleDimensionsLabel = new System.Windows.Forms.Label();
            this.sampleWidthLabel = new System.Windows.Forms.Label();
            this.sampleDepthLabel = new System.Windows.Forms.Label();
            this.sampleWidthText = new System.Windows.Forms.TextBox();
            this.sampleDepthText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calibrateParallelLineTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            this.chart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Cursor = System.Windows.Forms.Cursors.SizeAll;
            legend1.Name = "Legend1";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(174, 12);
            this.chart.Name = "chart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.MarkerSize = 3;
            series1.Name = "mainSeries";
            series2.BorderColor = System.Drawing.Color.Blue;
            series2.BorderWidth = 4;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Blue;
            series2.Legend = "Legend1";
            series2.MarkerBorderWidth = 10;
            series2.MarkerColor = System.Drawing.Color.Red;
            series2.MarkerSize = 10;
            series2.Name = "parallelLineSeries";
            series3.BorderWidth = 4;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            series3.Legend = "Legend1";
            series3.Name = "movedParallelLineSeries";
            this.chart.Series.Add(series1);
            this.chart.Series.Add(series2);
            this.chart.Series.Add(series3);
            this.chart.Size = new System.Drawing.Size(732, 484);
            this.chart.TabIndex = 1;
            this.chart.Text = "chart1";
            // 
            // selectFileBtn
            // 
            this.selectFileBtn.Location = new System.Drawing.Point(12, 12);
            this.selectFileBtn.Name = "selectFileBtn";
            this.selectFileBtn.Size = new System.Drawing.Size(126, 23);
            this.selectFileBtn.TabIndex = 3;
            this.selectFileBtn.Text = "Wybierz plik";
            this.selectFileBtn.UseVisualStyleBackColor = true;
            this.selectFileBtn.Click += new System.EventHandler(this.selectFileBtn_Click);
            // 
            // filePath
            // 
            this.filePath.AutoSize = true;
            this.filePath.Location = new System.Drawing.Point(12, 38);
            this.filePath.Name = "filePath";
            this.filePath.Size = new System.Drawing.Size(31, 13);
            this.filePath.TabIndex = 4;
            this.filePath.Text = "<....>";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 511);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(171, 533);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(416, 511);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(416, 533);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(652, 511);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(652, 533);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 10;
            // 
            // alloyTensileRadioButton
            // 
            this.alloyTensileRadioButton.AutoSize = true;
            this.alloyTensileRadioButton.Checked = true;
            this.alloyTensileRadioButton.Location = new System.Drawing.Point(6, 19);
            this.alloyTensileRadioButton.Name = "alloyTensileRadioButton";
            this.alloyTensileRadioButton.Size = new System.Drawing.Size(84, 17);
            this.alloyTensileRadioButton.TabIndex = 11;
            this.alloyTensileRadioButton.TabStop = true;
            this.alloyTensileRadioButton.Text = "Rozciaganie";
            this.alloyTensileRadioButton.UseVisualStyleBackColor = true;
            this.alloyTensileRadioButton.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // creepRadioButton
            // 
            this.creepRadioButton.AutoSize = true;
            this.creepRadioButton.Location = new System.Drawing.Point(6, 42);
            this.creepRadioButton.Name = "creepRadioButton";
            this.creepRadioButton.Size = new System.Drawing.Size(65, 17);
            this.creepRadioButton.TabIndex = 12;
            this.creepRadioButton.TabStop = true;
            this.creepRadioButton.Text = "Pelzanie";
            this.creepRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.alloyTensileRadioButton);
            this.groupBox1.Controls.Add(this.creepRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(126, 76);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Typ proby";
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(12, 163);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 14;
            this.calculateButton.Text = "Oblicz";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // calibrateParallelLineTrackBar
            // 
            this.calibrateParallelLineTrackBar.Location = new System.Drawing.Point(15, 381);
            this.calibrateParallelLineTrackBar.Minimum = 1;
            this.calibrateParallelLineTrackBar.Name = "calibrateParallelLineTrackBar";
            this.calibrateParallelLineTrackBar.Size = new System.Drawing.Size(104, 45);
            this.calibrateParallelLineTrackBar.TabIndex = 15;
            this.calibrateParallelLineTrackBar.Value = 1;
            this.calibrateParallelLineTrackBar.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // calibrateParallelLineLabel
            // 
            this.calibrateParallelLineLabel.AutoSize = true;
            this.calibrateParallelLineLabel.Location = new System.Drawing.Point(12, 365);
            this.calibrateParallelLineLabel.Name = "calibrateParallelLineLabel";
            this.calibrateParallelLineLabel.Size = new System.Drawing.Size(126, 13);
            this.calibrateParallelLineLabel.TabIndex = 16;
            this.calibrateParallelLineLabel.Text = "Kalibracja linii rownoleglej";
            // 
            // lengthBeforeText
            // 
            this.lengthBeforeText.Location = new System.Drawing.Point(12, 328);
            this.lengthBeforeText.Name = "lengthBeforeText";
            this.lengthBeforeText.Size = new System.Drawing.Size(100, 20);
            this.lengthBeforeText.TabIndex = 17;
            this.lengthBeforeText.Text = "0";
            // 
            // lengthBeforeLabel
            // 
            this.lengthBeforeLabel.AutoSize = true;
            this.lengthBeforeLabel.Location = new System.Drawing.Point(9, 312);
            this.lengthBeforeLabel.Name = "lengthBeforeLabel";
            this.lengthBeforeLabel.Size = new System.Drawing.Size(162, 13);
            this.lengthBeforeLabel.TabIndex = 18;
            this.lengthBeforeLabel.Text = "Dlugosc probki przed proba [mm]";
            // 
            // sampleDimensionsLabel
            // 
            this.sampleDimensionsLabel.AutoSize = true;
            this.sampleDimensionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sampleDimensionsLabel.Location = new System.Drawing.Point(9, 203);
            this.sampleDimensionsLabel.Name = "sampleDimensionsLabel";
            this.sampleDimensionsLabel.Size = new System.Drawing.Size(120, 16);
            this.sampleDimensionsLabel.TabIndex = 19;
            this.sampleDimensionsLabel.Text = "Wymiary probki:";
            // 
            // sampleWidthLabel
            // 
            this.sampleWidthLabel.AutoSize = true;
            this.sampleWidthLabel.Location = new System.Drawing.Point(9, 219);
            this.sampleWidthLabel.Name = "sampleWidthLabel";
            this.sampleWidthLabel.Size = new System.Drawing.Size(82, 13);
            this.sampleWidthLabel.TabIndex = 20;
            this.sampleWidthLabel.Text = "Szerokosc [mm]";
            // 
            // sampleDepthLabel
            // 
            this.sampleDepthLabel.AutoSize = true;
            this.sampleDepthLabel.Location = new System.Drawing.Point(9, 266);
            this.sampleDepthLabel.Name = "sampleDepthLabel";
            this.sampleDepthLabel.Size = new System.Drawing.Size(72, 13);
            this.sampleDepthLabel.TabIndex = 21;
            this.sampleDepthLabel.Text = "Grubosc [mm]";
            // 
            // sampleWidthText
            // 
            this.sampleWidthText.Location = new System.Drawing.Point(12, 236);
            this.sampleWidthText.Name = "sampleWidthText";
            this.sampleWidthText.Size = new System.Drawing.Size(100, 20);
            this.sampleWidthText.TabIndex = 22;
            this.sampleWidthText.Text = "0";
            // 
            // sampleDepthText
            // 
            this.sampleDepthText.Location = new System.Drawing.Point(12, 282);
            this.sampleDepthText.Name = "sampleDepthText";
            this.sampleDepthText.Size = new System.Drawing.Size(100, 20);
            this.sampleDepthText.TabIndex = 23;
            this.sampleDepthText.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 564);
            this.Controls.Add(this.sampleDepthText);
            this.Controls.Add(this.sampleWidthText);
            this.Controls.Add(this.sampleDepthLabel);
            this.Controls.Add(this.sampleWidthLabel);
            this.Controls.Add(this.sampleDimensionsLabel);
            this.Controls.Add(this.lengthBeforeLabel);
            this.Controls.Add(this.lengthBeforeText);
            this.Controls.Add(this.calibrateParallelLineLabel);
            this.Controls.Add(this.calibrateParallelLineTrackBar);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filePath);
            this.Controls.Add(this.selectFileBtn);
            this.Controls.Add(this.chart);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calibrateParallelLineTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.Button selectFileBtn;
        private System.Windows.Forms.Label filePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton alloyTensileRadioButton;
        private System.Windows.Forms.RadioButton creepRadioButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.TrackBar calibrateParallelLineTrackBar;
        private System.Windows.Forms.Label calibrateParallelLineLabel;
        private System.Windows.Forms.TextBox lengthBeforeText;
        private System.Windows.Forms.Label lengthBeforeLabel;
        private System.Windows.Forms.Label sampleDimensionsLabel;
        private System.Windows.Forms.Label sampleWidthLabel;
        private System.Windows.Forms.Label sampleDepthLabel;
        private System.Windows.Forms.TextBox sampleWidthText;
        private System.Windows.Forms.TextBox sampleDepthText;
    }
}

